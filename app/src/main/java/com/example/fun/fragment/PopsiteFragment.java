package com.example.fun.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fun.R;
import com.example.fun.activity.SearchActivity;
import com.example.fun.activity.WebViewActivity;
import com.example.fun.adapter.PopSiteAdapter;
import com.example.fun.bean.PopSiteBean;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PopsiteFragment extends Fragment {
    private static final String TAG = "PopsiteFragment";
    private RecyclerView rvCategory;
    private List<PopSiteBean.DataDTO> popSiteBeanList = new ArrayList<>();
    private PopSiteAdapter popSiteAdapter;
    private EditText etSearch;
    private String tvSearch;
    private Button btnSearch;

    public static Fragment newInstance() {
        return new PopsiteFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_popsite, container, false);
        rvCategory = view.findViewById(R.id.rv_popSite);
        etSearch = view.findViewById(R.id.et_search);
        btnSearch = view.findViewById(R.id.btn_search);

        new Thread(){
            @Override
            public void run(){
                initData();
            }
        }.start();

        initRvPopSite();
        ItemClick();
        return view;
    }

    private void initData() {
        String address = "https://www.wanandroid.com/friend/json";
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(address)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.d(TAG, "onFailure: 当前线程：" + Thread.currentThread().getName() + e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                Log.d(TAG, "onResponse: 当前线程：" + Thread.currentThread().getName());
                String content = response.body().string();
                Gson gson = new Gson();
                PopSiteBean popSiteBean = gson.fromJson(content, PopSiteBean.class);
                popSiteBeanList.addAll(popSiteBean.getData());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        popSiteAdapter.setList(popSiteBeanList);
                    }
                });
            }
        });
    }

    private void initRvPopSite(){
        rvCategory.setLayoutManager(new LinearLayoutManager(getActivity()));
        popSiteAdapter = new PopSiteAdapter(popSiteBeanList, getActivity());
        rvCategory.setAdapter(popSiteAdapter);
    }

    private void ItemClick(){

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvSearch = etSearch.getText().toString();
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                intent.putExtra("tvSearch", tvSearch);
                startActivity(intent);
            }
        });

        popSiteAdapter.setOnItemClickListener(new PopSiteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                List<PopSiteBean.DataDTO> list = popSiteAdapter.getList();
                WebViewActivity.gotoWebView(getActivity(), list.get(position).getLink());
            }
        });
    }
}
