package com.example.fun.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fun.R;
import com.example.fun.activity.WebViewActivity;
import com.example.fun.adapter.HomeAdapter;
import com.example.fun.adapter.QAAdapter;
import com.example.fun.bean.InfoBean;
import com.example.fun.bean.QABean;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class QaFragment extends Fragment {
    private static final String TAG = "QaFragment";
    private RecyclerView rvQA;
    private List<QABean.DataDTO.DatasDTO> qABeanList = new ArrayList<>();
    private QAAdapter qAAdapter;

    public static Fragment newInstance() {
        return new QaFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_qa, null);
        rvQA = view.findViewById(R.id.rv_qa);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initData();
        inRVQA();
        ItemClick();
    }

    private void initData() {
        String address = "https://wanandroid.com/wenda/list/1/json";
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(address)
                .build();
        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(@NonNull okhttp3.Call call, @NonNull IOException e) {
                Log.d(TAG, "onFailure: 当前线程：" + Thread.currentThread().getName()+e.getMessage());
            }

            @Override
            public void onResponse(@NonNull okhttp3.Call call, @NonNull Response response) throws IOException {
                Log.d(TAG, "onResponse: 当前线程：" + Thread.currentThread().getName());
                String content = response.body().string();
                Gson gson = new Gson();
                QABean qABean = gson.fromJson(content, QABean.class);
                qABeanList.addAll(qABean.getData().getDatas());
                //使用Activity提供的runOnUiThread()方法切换线程
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        qAAdapter.setList(qABeanList);
                    }
                });
            }
        });
    }


    private void inRVQA(){
        rvQA.setLayoutManager(new LinearLayoutManager(getActivity()));
        qAAdapter = new QAAdapter(qABeanList, getActivity());
        rvQA.setAdapter(qAAdapter);

    }
    private void ItemClick(){
        qAAdapter.setOnItemClickListener(new QAAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                List<QABean.DataDTO.DatasDTO> list = qAAdapter.getList();
                WebViewActivity.gotoWebView(getActivity(), list.get(position).getLink());

            }
        });
    }
}
