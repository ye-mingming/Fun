package com.example.fun.activity;

import android.content.Context;

import org.litepal.LitePalApplication;

public class Application extends LitePalApplication {
    public static Context instance;

    @Override
    public void onCreate(){
        super.onCreate();
        instance = this;
    }

}
