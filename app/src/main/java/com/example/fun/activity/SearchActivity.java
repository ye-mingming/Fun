package com.example.fun.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.fun.R;
import com.example.fun.adapter.SearchAdapter;
import com.example.fun.bean.SearchBean;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SearchActivity extends AppCompatActivity {
    private static final String TAG = "SearchActivity";
    private List<SearchBean.DataDTO.DatasDTO> list = new ArrayList<>();
    private RecyclerView rvSearch;
    private SearchAdapter searchAdapter;
    private String tvSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        rvSearch = findViewById(R.id.rv_search);
        rvSearch.setLayoutManager(new LinearLayoutManager(this));
        searchAdapter = new SearchAdapter(list, this);
        rvSearch.setAdapter(searchAdapter);

        Intent intent = getIntent();
        tvSearch = intent.getStringExtra("tvSearch");

        new Thread() {
            @Override
            public void run() {
                getSearchData();
            }
        }.start();

        searchAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                List<SearchBean.DataDTO.DatasDTO> list = searchAdapter.getList();
                WebViewActivity.gotoWebView(SearchActivity.this, list.get(position).getLink());
            }
        });
    }

    private void getSearchData() {
        String address = "https://www.wanandroid.com/article/query/0/json";

        OkHttpClient client = new OkHttpClient.Builder().build();
        FormBody body = new FormBody.Builder().add("k", tvSearch).build();
        Request request = new Request.Builder().post(body).url(address).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Toast.makeText(SearchActivity.this, "搜索失败，请检查网络", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String searchBody = response.body().string();
                Log.d(TAG, "服务端响应: " + searchBody);
                if (!response.isSuccessful()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SearchActivity.this, "请求异常" + response.code(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    return;
                }
                Gson gson = new Gson();
                SearchBean searchBean = gson.fromJson(searchBody, SearchBean.class);
                //Log.d(TAG, "onResponse: " + searchBean);

                if (searchBean.getData().getDatas().size() != 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            list.addAll(searchBean.getData().getDatas());
                            searchAdapter.notifyDataSetChanged();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SearchActivity.this, "找不到相关内容，请重新输入关键词！", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }


}
