package com.example.fun.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.fun.R;
import com.example.fun.adapter.CollectAdapter;
import com.example.fun.bean.CollectBean;
import com.example.fun.bean.InfoBean;
import com.example.fun.fragment.HomeFragment;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CollectionActivity extends Activity {

    private static final String TAG = "CollectionActivity";
    private RecyclerView rvCollect;
    private List<CollectBean.DataDTO.DatasDTO> list = new ArrayList<>();
    private CollectAdapter collectAdapter;
    private int currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);

        rvCollect = findViewById(R.id.rv_collection);
        rvCollect.setLayoutManager(new LinearLayoutManager(this));
        collectAdapter = new CollectAdapter(list, this);
        rvCollect.setAdapter(collectAdapter);

        new Thread() {
            @Override
            public void run() {
                getCollectData();
            }
        }.start();

        collectAdapter.setOnItemClickListener(new CollectAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                List<CollectBean.DataDTO.DatasDTO> list = collectAdapter.getList();
                WebViewActivity.gotoWebView(CollectionActivity.this, list.get(position).getLink());
            }


        });

        collectAdapter.setOnCollectListener(new CollectAdapter.OnCollectListener() {
            @Override
            public void onCollectClick(View view, int position) {
                currentPosition = position;
                getCollect(position);
            }
        });
    }

    private void getCollectData() {
        String address = "https://www.wanandroid.com/lg/collect/list/0/json";

        OkHttpClient client = new OkHttpClient.Builder()
                .cookieJar(HomeFragment.getCookieJar())
                .build();
        Request request = new Request.Builder()
                .url(address)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Toast.makeText(CollectionActivity.this, "连接网络出错，请重新尝试！", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String collectBody = response.body().string();
                Gson gson = new Gson();
                CollectBean collectBean = gson.fromJson(collectBody, CollectBean.class);
                if (response.code() >= 200) {
                    if (collectBean.getData().getDatas().size() != 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                list.addAll(collectBean.getData().getDatas());
                                collectAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CollectionActivity.this, "该用户没有收藏", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            }
        });
    }

    private void getCollect(int collectPosition) {
        String address;
        CollectBean.DataDTO.DatasDTO entity = collectAdapter.getList().get(collectPosition);
        address = "https://www.wanandroid.com/lg/uncollect_originId/" + entity.getId() + "/json";

        Log.d(TAG, "getCollect: address" + address);
        OkHttpClient client = new OkHttpClient.Builder().cookieJar(HomeFragment.getCookieJar()).build();
        FormBody body = new FormBody.Builder().build();
        Request request = new Request.Builder().post(body).url(address).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CollectionActivity.this, "取消失败，请检查网络！", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String collectBody = response.body().string();
                Gson gson = new Gson();
                InfoBean infoBean = gson.fromJson(collectBody, InfoBean.class);

                if (infoBean.getErrorCode() == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CollectionActivity.this, "取消收藏成功！", Toast.LENGTH_LONG).show();
                            collectAdapter.getList().remove(currentPosition);
                            collectAdapter.notifyItemRemoved(currentPosition);
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CollectionActivity.this, infoBean.getErrorCode() + "", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

    }


}
