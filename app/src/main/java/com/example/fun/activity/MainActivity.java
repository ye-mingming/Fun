package com.example.fun.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.fun.R;
import com.example.fun.adapter.MainAdapter;
import com.example.fun.fragment.HomeFragment;
import com.example.fun.fragment.MineFragment;
import com.example.fun.fragment.PopsiteFragment;
import com.example.fun.fragment.QaFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private TextView tvHome;
    private TextView tvQa;
    private TextView tvPopSite;
    private TextView tvMine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        initViewPaper();

        initListener();

        onNewIntent(getIntent());

    }

    @Override
    protected void onNewIntent(Intent intent){
        super.onNewIntent(intent);
        int mFlag = intent.getIntExtra("flag", 0);
        if(mFlag == 3){
            viewPager.setCurrentItem(3);
        }
    }

    private void initListener(){
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        tvHome.setSelected(true);
                        tvHome.setBackgroundColor(getResources().getColor(R.color.green));
                        tvHome.setTextColor(getResources().getColor(R.color.white));
                        tvQa.setSelected(false);
                        tvQa.setBackgroundColor(getResources().getColor(R.color.white));
                        tvQa.setTextColor(getResources().getColor(R.color.green));
                        tvPopSite.setSelected(false);
                        tvPopSite.setBackgroundColor(getResources().getColor(R.color.white));
                        tvPopSite.setTextColor(getResources().getColor(R.color.green));
                        tvMine.setSelected(false);
                        tvMine.setBackgroundColor(getResources().getColor(R.color.white));
                        tvMine.setTextColor(getResources().getColor(R.color.green));
                        break;
                    case 1:
                        tvHome.setSelected(false);
                        tvHome.setBackgroundColor(getResources().getColor(R.color.white));
                        tvHome.setTextColor(getResources().getColor(R.color.green));
                        tvQa.setSelected(true);
                        tvQa.setBackgroundColor(getResources().getColor(R.color.green));
                        tvQa.setTextColor(getResources().getColor(R.color.white));
                        tvPopSite.setSelected(false);
                        tvPopSite.setBackgroundColor(getResources().getColor(R.color.white));
                        tvPopSite.setTextColor(getResources().getColor(R.color.green));
                        tvMine.setSelected(false);
                        tvMine.setBackgroundColor(getResources().getColor(R.color.white));
                        tvMine.setTextColor(getResources().getColor(R.color.green));
                        break;
                    case 2:
                        tvHome.setSelected(false);
                        tvHome.setBackgroundColor(getResources().getColor(R.color.white));
                        tvHome.setTextColor(getResources().getColor(R.color.green));
                        tvQa.setSelected(false);
                        tvQa.setBackgroundColor(getResources().getColor(R.color.white));
                        tvQa.setTextColor(getResources().getColor(R.color.green));
                        tvPopSite.setSelected(true);
                        tvPopSite.setBackgroundColor(getResources().getColor(R.color.green));
                        tvPopSite.setTextColor(getResources().getColor(R.color.white));
                        tvMine.setSelected(false);
                        tvMine.setBackgroundColor(getResources().getColor(R.color.white));
                        tvMine.setTextColor(getResources().getColor(R.color.green));
                        break;
                    case 3:
                        tvHome.setSelected(false);
                        tvHome.setBackgroundColor(getResources().getColor(R.color.white));
                        tvHome.setTextColor(getResources().getColor(R.color.green));
                        tvQa.setSelected(false);
                        tvQa.setBackgroundColor(getResources().getColor(R.color.white));
                        tvQa.setTextColor(getResources().getColor(R.color.green));
                        tvPopSite.setSelected(false);
                        tvPopSite.setBackgroundColor(getResources().getColor(R.color.white));
                        tvPopSite.setTextColor(getResources().getColor(R.color.green));
                        tvMine.setSelected(true);
                        tvMine.setBackgroundColor(getResources().getColor(R.color.green));
                        tvMine.setTextColor(getResources().getColor(R.color.white));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tvHome.setSelected(true);
        tvHome.setBackgroundColor(getResources().getColor(R.color.green));
        tvHome.setTextColor(getResources().getColor(R.color.white));
    }

    private void initViewPaper(){
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(HomeFragment.newInstance());
        fragmentList.add(QaFragment.newInstance());
        fragmentList.add(PopsiteFragment.newInstance());
        fragmentList.add(MineFragment.newInstance());

        MainAdapter mainAdapter = new MainAdapter(getSupportFragmentManager(), fragmentList);
        viewPager.setAdapter(mainAdapter);
        viewPager.setCurrentItem(0);
        tvHome.setSelected(true);
    }

    private void initView(){
        viewPager = findViewById(R.id.viewPager);
        tvHome = findViewById(R.id.tv_home);
        tvQa = findViewById(R.id.tv_qa);
        tvPopSite = findViewById(R.id.tv_popSite);
        tvMine = findViewById(R.id.tv_mine);
    }

    public void gotoHome(View view){
        viewPager.setCurrentItem(0);
    }

    public void gotoQa(View view) {
        viewPager.setCurrentItem(1);
    }

    public void gotoPopSite(View view) {
        viewPager.setCurrentItem(2);
    }

    public void gotoMine(View view) {
        viewPager.setCurrentItem(3);
    }
}