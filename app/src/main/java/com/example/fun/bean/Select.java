package com.example.fun.bean;

public class Select {
    public String content;
    public boolean isSelected;

    public Select(String content, boolean isSelected){
        this.content = content;
        this.isSelected = isSelected;
    }
}
