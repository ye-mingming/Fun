package com.example.fun.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fun.R;
import com.example.fun.bean.InfoBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder> {
    private List<InfoBean.DataDTO.DatasDTO> list = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
    private OnItemClickListener onItemClickListener;
    private OnCollectListener onCollectListener;

    public void setList(List<InfoBean.DataDTO.DatasDTO> list) {
        if (list != null) {
            this.list.addAll(list);
        }
        notifyDataSetChanged();
    }

    public HomeAdapter(List<InfoBean.DataDTO.DatasDTO> list, Context context) {
        if (list != null) {
            this.list.addAll(list);
        }
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_home, parent, false);
        HomeViewHolder homeViewHolder = new HomeViewHolder(view);
        return homeViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        InfoBean.DataDTO.DatasDTO infoBean = list.get(position);
        String author = new String(infoBean.getAuthor());
        if(author.length() != 0){
            holder.tvAuthor.setText(author);
        }else {
            holder.tvAuthor.setText("匿名");
        }


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        将时间戳转化为日期格式
        Date date = new Date(infoBean.getPublishTime());
        holder.tvDate.setText(format.format(date));

//        把2022-08-12 17:52:00转成时间戳
//        String timeStr="2022-08-12 17:52:00";
//        try {
//            Date date1=format.parse(timeStr);
//          long timeMillis=  date1.getTime();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

        holder.tvTitle.setText(infoBean.getTitle());
        holder.tvWeb.setText(infoBean.getSuperChapterName() + "·" + infoBean.getChapterName());

        boolean collect = infoBean.isCollect();
        if (collect){
            holder.imgCollect.setImageResource(R.drawable.collect_yellow);
        }else {
            holder.imgCollect.setImageResource(R.drawable.collect_gray);
        }

        int adapterPosition = holder.getAdapterPosition();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemClickListener != null){
                    onItemClickListener.onItemClick(holder.itemView, adapterPosition);
                }
            }
        });

        holder.imgCollect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onCollectListener != null){
                    onCollectListener.onCollectClick(view, adapterPosition);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public List<InfoBean.DataDTO.DatasDTO> getList() {
        return list;
    }


    static class HomeViewHolder extends RecyclerView.ViewHolder {
        private View itemView;
        TextView tvAuthor;
        TextView tvDate;
        TextView tvTitle;
        TextView tvWeb;
        ImageView imgCollect;

        public HomeViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            tvAuthor = itemView.findViewById(R.id.tv_author);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvWeb = itemView.findViewById(R.id.tv_web);
            imgCollect = itemView.findViewById(R.id.img_collect);
        }
    }

    /**
     * 自定义item单击监听器
     */
    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    /**
     * 自定义收藏控件的单击监听器
     */
    public interface OnCollectListener{
        void onCollectClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnCollectListener(OnCollectListener onCollectListener){
        this.onCollectListener = onCollectListener;
    }

}
