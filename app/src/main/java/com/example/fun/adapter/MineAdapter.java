package com.example.fun.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fun.R;
import com.example.fun.bean.MineBean;

import java.util.ArrayList;
import java.util.List;

public class MineAdapter extends RecyclerView.Adapter<MineAdapter.MineViewHolder> {
    private List<MineBean.DataDTO.UserInfoDTO> list = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
    private OnItemClickLiserner onItemClickListerner;

    public void setList(List<MineBean.DataDTO.UserInfoDTO> list){
        if(list != null){
            this.list.addAll(list);
        }notifyDataSetChanged();
    }

    public MineAdapter(List<MineBean.DataDTO.UserInfoDTO> list, Context context){
        if(list != null){
            this.list.addAll(list);
        }
        inflater = LayoutInflater.from(context);
        this.context = context;
    }
    @NonNull
    @Override
    public MineAdapter.MineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_mine, null, false);
        MineViewHolder mineViewHolder = new MineViewHolder(view);
        return mineViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MineAdapter.MineViewHolder holder, int position) {
        MineBean.DataDTO.UserInfoDTO mineBean = list.get(position);
        String icon = new String((mineBean.getIcon()));
        if(icon == null){
            holder.Icon.setImageResource(R.drawable.icon_register);
        }
//        holder.Icon.setImageResource(R.drawable.arrow_right);

        holder.tvUserName.setText(mineBean.getUsername());
        holder.tvCoin.setText(mineBean.getCoinCount());
        holder.tvNickName.setText(mineBean.getNickname());

        int adapterPosition = holder.getAdapterPosition();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemClickListerner != null){
                    onItemClickListerner.onItemClick(holder.itemView, adapterPosition);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


    public class MineViewHolder extends RecyclerView.ViewHolder {
        ImageView Icon;
        TextView tvUserName;
        TextView tvCoin;
        TextView tvNickName;

        public MineViewHolder(@NonNull View itemView) {
            super(itemView);
            Icon = itemView.findViewById(R.id.img_goto);
            tvUserName = itemView.findViewById(R.id.tv_goLogin);
            tvCoin = itemView.findViewById(R.id.tv_coin);
            tvNickName = itemView.findViewById(R.id.tv_aboutAuthor);
        }
    }

    public interface OnItemClickLiserner{
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListerner(OnItemClickLiserner onItemClickListerner){
        this.onItemClickListerner = onItemClickListerner;
    }
}
