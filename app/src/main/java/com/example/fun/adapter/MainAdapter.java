package com.example.fun.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public class MainAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> list;

    public MainAdapter(@NonNull FragmentManager fragmentManager, List<Fragment> list){
        super(fragmentManager);
        this.list = list;
    }

    @NonNull
    @Override
    public Fragment getItem(int position){
        return list.get(position);
    }

    @Override
    public int getCount(){
        return list.size();
    }

    /*
    自定义item监听器
     */
    public interface OnItemOnClickListener{
        void onItemClick(View view, int position);
    }
}
