package com.example.fun.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fun.R;
import com.example.fun.bean.SearchBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {
    private List<SearchBean.DataDTO.DatasDTO> list = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
    private OnItemClickListener onItemClickListener;

    public void setList(List<SearchBean.DataDTO.DatasDTO> list){
        if(list != null){
            this.list.addAll(list);
        }
        notifyDataSetChanged();
    }

    public SearchAdapter(List<SearchBean.DataDTO.DatasDTO> list, Context context){
        if(list != null){
            this.list.addAll(list);
        }
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_search, parent, false);
        SearchViewHolder searchViewHolder = new SearchViewHolder(view);
        return searchViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAdapter.SearchViewHolder holder, int position) {
        SearchBean.DataDTO.DatasDTO searchBean = list.get(position);
        String author = new String(searchBean.getAuthor());
        if(author.length() != 0){
            holder.tvAuthor.setText(author);
        }else {
            holder.tvAuthor.setText("匿名");
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        将时间戳转化为日期格式
        Date date = new Date(searchBean.getPublishTime());
        holder.tvDate.setText(format.format(date));

        holder.tvTitle.setText(searchBean.getTitle());
        holder.tvWeb.setText(searchBean.getSuperChapterName() + "·" + searchBean.getChapterName());

        int adapterPosition = holder.getAdapterPosition();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemClickListener != null){
                    onItemClickListener.onItemClick(holder.itemView, adapterPosition);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public List<SearchBean.DataDTO.DatasDTO> getList(){
        return list;
    }

    static class SearchViewHolder extends RecyclerView.ViewHolder{
        private View itemView;
        TextView tvAuthor;
        TextView tvDate;
        TextView tvTitle;
        TextView tvWeb;

        public SearchViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            tvAuthor = itemView.findViewById(R.id.tv_author);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvWeb = itemView.findViewById(R.id.tv_web);
        }
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}
