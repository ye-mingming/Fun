package com.example.fun.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fun.R;
import com.example.fun.bean.QABean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QAAdapter extends RecyclerView.Adapter<QAAdapter.QAViewHolder> {

    private List<QABean.DataDTO.DatasDTO> list = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
    private OnItemClickListener onItemClickListener;

    public void setList(List<QABean.DataDTO.DatasDTO> list) {
        if (list != null) {
            this.list.addAll(list);
        }
        notifyDataSetChanged();
    }

    public QAAdapter(List<QABean.DataDTO.DatasDTO> list, Context context) {
        if (list != null) {
            this.list.addAll(list);
        }
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public QAViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_qa, parent, false);
        QAViewHolder qAViewHolder = new QAViewHolder(view);
        return qAViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull QAViewHolder holder, int position) {
        QABean.DataDTO.DatasDTO qABean = list.get(position);
        String author = new String(qABean.getAuthor());
        if (author.length() != 0) {
            holder.tvAuthor.setText(author);
        } else {
            holder.tvAuthor.setText("匿名");
        }

        holder.tvTagsName.setText(qABean.getTags().get(0).getName());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = new Date(qABean.getPublishTime());
        holder.tvTime.setText(format.format(date));

        holder.tvTitle.setText(qABean.getTitle());
        holder.tvDesc.setText(qABean.getDesc());
        holder.tvSuperChaterName.setText(qABean.getSuperChapterName() + ":");
        holder.tvChapterName.setText(qABean.getChapterName());

        int adapterPosition = holder.getAdapterPosition();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemClickListener != null){
                    onItemClickListener.onItemClick(holder.itemView, adapterPosition);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public List<QABean.DataDTO.DatasDTO> getList() {
        return list;
    }


    static class QAViewHolder extends RecyclerView.ViewHolder {
        private View itemView;
        TextView tvAuthor;
        TextView tvTagsName;
        TextView tvTime;
        TextView tvTitle;
        TextView tvDesc;
        TextView tvSuperChaterName;
        TextView tvChapterName;

        public QAViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            tvAuthor = itemView.findViewById(R.id.tv_author);
            tvTagsName = itemView.findViewById(R.id.tv_tagName);
            tvTime = itemView.findViewById(R.id.tv_time);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDesc = itemView.findViewById(R.id.tv_desc);
            tvSuperChaterName = itemView.findViewById(R.id.tv_superChapterName);
            tvChapterName = itemView.findViewById(R.id.tv_chapterName);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
