package com.example.fun.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fun.R;
import com.example.fun.bean.CoinBean;

import java.util.ArrayList;
import java.util.List;

public class CoinAdapter extends RecyclerView.Adapter<CoinAdapter.CoinViewHolder> {
    private List<CoinBean.DataDTO> list = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
//    private OnItemClickListerner onItemClickListerner;

//    public void setList(List<CoinBean.DataDTO> list){
//        if(list != null){
//            this.list.addAll(list);
//        }
//        notifyDataSetChanged();
//    }
//
//    public CoinAdapter(List<CoinBean.DataDTO> list, Context context){
//        if(list != null){
//            this.list.addAll(list);
//        }
//        inflater = LayoutInflater.from(context);
//        this.context = context;
//    }

    @NonNull
    @Override
    public CoinViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_mine, parent, false);
//        CoinViewHolder coinViewHolder = new CoinViewHolder(view);
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull CoinAdapter.CoinViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class CoinViewHolder extends RecyclerView.ViewHolder {
        public CoinViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
